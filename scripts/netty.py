import sys
import socket
import time

def send(addr):
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

    for i in range(1, 10001):
        data = i.to_bytes(4, 'big')
        sock.sendto(data, (addr, 1234))
        

def recv():
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    sock.bind( ('', 1234) )
    received = 0
    while True:
        x, y = sock.recvfrom(1024)
        received += 1
        value = int.from_bytes(x, 'big')
        print("{}/{}".format(received,value))

if __name__ == '__main__':
    if sys.argv[1] == "recv":
        recv()
    else:
        send(sys.argv[2])
