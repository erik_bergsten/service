from mininet.topo import Topo
from mininet.net import Mininet
from mininet.cli import CLI
from mininet.term import makeTerm, cleanUpScreens # Open xterm from mininet
from mininet.link import TCLink, TCIntf, Intf # Customisable links & interfaces
import sys
import random

#
# Topology: every host connected to a switch
#
# h1       h4
#   \      /
#    \    /
#     \  /
# h2--NAT0--h5
#     /  \
#    /    \
#   /      \
# h3       etc....
#
#  and link parameters are the same for each link!
# only argument to the topo is n (the number of hosts)

#COMMAND = "bash"
#COMMAND = "./target/debug/examples/broadcast mininet {} {} ; read"
COMMAND = "./target/debug/examples/caster mininet {} {} 2>&1 | tee logs/{}_log_{}.csv"
#VALGRIND = "valgrind -- ./target/debug/examples/broadcast mininet {} {} ; read"

#bandwidth = 1 #Mbps
packetloss = 1.0 # % packetloss (per link!)
delay = lambda: 10+int(random.random()*50) # ms delay (per link!) RTT = 4*delay in this topology
class Topology( Topo ):

    def __init__(self, n):
        Topo.__init__(self)

        self.n = n
        switch = self.addSwitch('s1')

        for i in range(0, n):
            host = self.addHost("h{}".format(i))
            self.addLink(host,switch,loss=packetloss,delay="%dms"%delay())
    def command(self, log_prefix):
        return COMMAND.format(self.n, "{}", log_prefix, "{}")


topos = {'testtopo': (lambda: Topology())}



if __name__ == '__main__':
    topo = Topology(int(sys.argv[1]))
    log_prefix = sys.argv[2]
    net = Mininet(topo = topo, link=TCLink)
    net.addNAT().configDefault()
    net.start()
    for host in net.hosts:
        id = host.IP().replace('10.0.0.', '')
        if not host.name.startswith("nat"):
            makeTerm(node=host, cmd=topo.command(log_prefix).format(id, id))
        #makeTerm(node=host, cmd="bash")
    CLI(net)
    print("Stopping network....")
    net.stop()
