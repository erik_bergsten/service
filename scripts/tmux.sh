cmd="./target/debug/examples/$1 local 4"
cmd0="$cmd 3"
cmd1="$cmd 4"
tmux split-window -h "tmux split-window $cmd0; $cmd1"
tmux select-pane -L
tmux split-window "$cmd 2"
$cmd 1
tmux select-pane -R
tmux select-pane -U
