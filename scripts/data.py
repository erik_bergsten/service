import csv

def read_log(base, run, host):
    name = "logs/{}_{}_log_{}.csv".format(run, base, host)
    with open(name, newline='') as f:
        reader = csv.DictReader(f)
        data = []
        for row in reader:
            data.append(row)
        return data

def read_logs(base, run):
    datas = []
    for i in range(0,run):
        data = read_log(base, run, i+1)
        datas.append(data)
    return datas

def gen_out(base, to=10):
    out_name = "data/"+base+"_out.csv"
    with open(out_name, 'w', newline='') as f:
        writer = csv.writer(f, delimiter=',')
        for run in range(1, to+1):
            datas = read_logs(base, run)
            delay = average_delay(datas[0])
            total_messages = total_msgs_per_broadcast(datas)
            print("{}, {}, {}".format(run, delay, total_messages))
            writer.writerow([run, 0, delay, total_messages])
        f.close()



def average(data):
    average = data[0]
    for i in range(1, len(data)):
        average = (average*i + data[i])/(i+1)
    return average

def average_delay(data):
    return average(list(map(lambda x: int(x['delay']), data)))

def average_average_delay(datas):
    return average(list(map(average_delay, datas)))

def msgs_per_broadcast(data):
    msgs = int(data[-1]['sent'])
    return msgs / len(data)

def average_msgs_per_broadcast(datas):
    return average(list(map(msgs_per_broadcast, datas)))

def total_msgs_per_broadcast(datas):
    return sum(map(msgs_per_broadcast, datas))

