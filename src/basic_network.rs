use crate::dispatch::Dispatch;
use crate::hosts::Hosts;
use crate::id;
use crate::message::Message;
use crate::network::{NetworkHandler, Network};
use crate::vessel::{EventHandler, Service};
use std::io::Result;
use std::net::UdpSocket;
use std::sync::mpsc::{channel, Receiver, Sender};
use std::thread;

#[derive(Debug)]
pub enum NetworkMessage {
    Send(u16, Message),
    Broadcast(Message),
}

/// A basic network which operates from a pre-defined list of hosts.
/// Implements the network trait and broadcast means literally just that -
/// send to every host in the provided list.
pub struct BasicNetwork<'a> {
    network_tx: Sender<NetworkMessage>,
    dispatch: Dispatch<&'a dyn NetworkHandler>,
    n: u16
}
impl Service for BasicNetwork<'_> {
    fn id(&self) -> u16 {
        id::NETWORK
    }
    fn name(&self) -> &str {
        "Basic Network"
    }
}
impl EventHandler for BasicNetwork<'_> {
    fn on_event(&self, from: u16, mut msg: Message) {
        // TODO: error check message.pop_u16()
        let service_id = msg.pop_u16().unwrap();
        match self.dispatch.get(&service_id) {
            Some(service) => {
                service.on_message(from, msg);
            }
            None => {
                println!("[Network]: Invalid service id: {}", service_id);
            }
        }
    }
}
fn run_incoming(hosts: Hosts, socket: UdpSocket, event_tx: Sender<(u16, Message)>) {
    thread::spawn(move || {
        let mut buf: [u8; 256] = [0; 256];
        loop {
            let (len, from) = socket.recv_from(&mut buf).unwrap();
            match hosts.addr2id.get(&from) {
                None => {
                    println!("Received {:?} from unknown host: {:?}", &buf[..len], from);
                    let mut msg = Message::from_bytes(&buf[..len]);
                    msg.push_u16(id::NETWORK);
                    event_tx.send((0, msg)).unwrap();
                }
                Some(id) => {
                    //println!("[Incoming]: {:?} from {:?}", &buf[..len], id);
                    let mut msg = Message::from_bytes(&buf[..len]);
                    msg.push_u16(id::NETWORK);
                    event_tx.send((*id, msg)).unwrap();
                }
            }
        }
    });
}
fn run_outgoing(hosts: Hosts, socket: UdpSocket, network_rx: Receiver<NetworkMessage>) {
    thread::spawn(move || {
        loop {
            match network_rx.recv() {
                Ok(NetworkMessage::Send(to, message)) => {
                    //println!("[Outgoing]: {:?}", message);
                    let host = hosts.id2addr.get(&to).unwrap();
                    socket.send_to(message.as_bytes(), host).unwrap();
                }
                Ok(NetworkMessage::Broadcast(message)) => {
                    //println!("[Outgoing]: broadcacsting {:?}", message);
                    for (_, host) in hosts.id2addr.iter() {
                        socket.send_to(message.as_bytes(), host).unwrap();
                    }
                }
                _ => {
                    println!("[Network/OUTGOING]: Bad NetworkMessage read.");
                }
            }
        }
    });
}
fn start_threads(event_tx: Sender<(u16, Message)>, hosts: Hosts) -> Result<Sender<NetworkMessage>> {
    let (network_tx, network_rx) = channel();
    let socket = UdpSocket::bind(hosts.my_addr())?;
    let socket_ref = socket.try_clone()?;
    run_incoming(hosts.clone(), socket, event_tx);
    run_outgoing(hosts, socket_ref, network_rx);

    Ok(network_tx)
}
impl<'a> BasicNetwork<'a> {
    /// This creates a network object and starts the necessary threads, for
    /// sending and receiving messages. Can fail because of the system calls
    /// involved in networking.
    pub fn new(event_tx: Sender<(u16, Message)>, hosts: Hosts) -> Result<BasicNetwork<'a>> {
        let n = hosts.n;
        let network_tx = start_threads(event_tx, hosts)?;
        Ok(BasicNetwork {
            dispatch: Dispatch::new(),
            network_tx,
            n
        })
    }
    pub fn register(&self, service: &'a dyn NetworkHandler) {
        self.dispatch.register(service.id(), service);
    }
}
impl Network for BasicNetwork<'_> {
    fn send(&self, to: u16, mut msg: Message, service_id: u16) {
        msg.push_u16(service_id);
        self.network_tx.send(NetworkMessage::Send(to, msg)).unwrap();
    }
    fn broadcast(&self, mut msg: Message, service_id: u16) {
        msg.push_u16(service_id);
        self.network_tx
            .send(NetworkMessage::Broadcast(msg))
            .unwrap();
    }
    fn n(&self) -> u16 {
        self.n
    }
}
