use std::collections::HashMap;
use std::net::{IpAddr, Ipv4Addr, SocketAddr};
use std::result;
use std::error::Error;
use std::fs;

type Result<T> = result::Result<T, Box<dyn Error>>;

#[derive(Clone)]
pub struct Hosts {
    pub id: u16,
    pub n: u16,
    pub id2addr: HashMap<u16, SocketAddr>,
    pub addr2id: HashMap<SocketAddr, u16>,
}

impl Hosts {
    pub fn new_localhosts(id: u16, n: u16) -> Hosts {
        let mut id2addr = HashMap::new();
        let mut addr2id = HashMap::new();
        let localhost = IpAddr::V4(Ipv4Addr::new(127, 0, 0, 1));
        for i in 0..n {
            let port: u16 = 4000 + i;
            let addr = SocketAddr::new(localhost, port);
            id2addr.insert(i, addr);
            addr2id.insert(addr, i);
        }
        Hosts {
            id,
            n,
            id2addr,
            addr2id,
        }
    }
    pub fn new_minihosts(id: u16, n: u16) -> Hosts {
        let mut id2addr = HashMap::new();
        let mut addr2id = HashMap::new();
        for i in 0..n {
            let port: u16 = 4444;
            let host = IpAddr::V4(Ipv4Addr::new(10, 0, 0, (i+1) as u8));
            let addr = SocketAddr::new(host, port);
            id2addr.insert(i, addr);
            addr2id.insert(addr, i);
        }
        Hosts {
            id,
            n,
            id2addr,
            addr2id,
        }
    }
    pub fn load_hosts(id: u16, filename: &str) -> Result<Hosts> {
        let mut id2addr = HashMap::new();
        let mut addr2id = HashMap::new();
        let text = fs::read_to_string(filename).unwrap();
        let mut n: u16 = 0;
        for (i,  line) in text.lines().enumerate() {
            n += 1;
            let mut address: SocketAddr = line.parse().unwrap();
            if (i as u16) == id {
                let any = IpAddr::V4(Ipv4Addr::new(0, 0, 0, 0));
                address = SocketAddr::new(any, 4444);
            }
            id2addr.insert(i as u16, address);
            addr2id.insert(address, i as u16);
        }
        Ok(Hosts {
            id,
            n,
            id2addr,
            addr2id
        })
    }
    pub fn show(&self) {
        for (id, addr) in self.id2addr.iter() {
            println!("[{}]: {:?}", id, addr);
        }
    }
    pub fn my_addr(&self) -> SocketAddr {
        *self.id2addr.get(&self.id).unwrap()
    }
}
