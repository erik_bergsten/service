use serde::{Deserialize,Serialize};
use serde::de::DeserializeOwned;
use std::io::Write;
#[derive(Debug, Clone, PartialEq, Eq, Hash, Deserialize,Serialize)]
pub struct Message(Vec<u8>);
impl Message {
    pub fn empty() -> Self {
        Message(Vec::new())
    }
    pub fn from_bytes(text: &[u8]) -> Self {
        Message(text.to_vec())
    }
    pub fn from_string(text: String) -> Self {
        Message(text.into_bytes())
    }
    pub fn from_str(text: &str) -> Self {
        Message(text.as_bytes().to_vec())
    }
    pub fn push_u32(&mut self, value: u32) {
        let [b1, b2, b3, b4] = value.to_be_bytes();
        self.0.push(b1);
        self.0.push(b2);
        self.0.push(b3);
        self.0.push(b4);
    }
    pub fn pop_u32(&mut self) -> Option<u32> {
        let b4 = self.0.pop()?;
        let b3 = self.0.pop()?;
        let b2 = self.0.pop()?;
        let b1 = self.0.pop()?;
        Some(u32::from_be_bytes([b1, b2, b3, b4]))
    }
    pub fn push_u16(&mut self, value: u16) {
        let [b1, b2] = value.to_be_bytes();
        self.0.push(b1);
        self.0.push(b2);
    }
    pub fn peek_u16(&self) -> u16 {
        let b2 = self.0[self.0.len() - 1];
        let b1 = self.0[self.0.len() - 2];
        u16::from_be_bytes([b1, b2])
    }
    pub fn pop_u16(&mut self) -> Option<u16> {
        let b2 = self.0.pop()?;
        let b1 = self.0.pop()?;
        Some(u16::from_be_bytes([b1, b2]))
    }
    pub fn push_u8(&mut self, value: u8) {
        self.0.push(value);
    }
    pub fn pop_u8(&mut self) -> Option<u8> {
        self.0.pop()
    }
    pub fn peek_u8(&self) -> u8 {
        self.0[self.0.len() - 1]
    }
    pub fn to_string(msg: Message) -> String {
        String::from_utf8(msg.0).unwrap()
    }
    pub fn as_str(&self) -> &str {
        std::str::from_utf8(&self.0[..]).unwrap()
    }
    pub fn as_bytes(&self) -> &[u8] {
        &self.0[..]
    }
    /*
    pub fn push_json<T: Serialize>(&mut self, msg: &T) {
        let mut encoded = serde_json::to_vec(msg).unwrap();
        let length = encoded.len();
        self.0.append(&mut encoded);
        self.push_u16(length as u16);
    }
    pub fn pop_json<T: DeserializeOwned>(&mut self) -> T {
        let length = self.pop_u16().unwrap();
        let raw = self.0.split_off(self.0.len() - length as usize);
        serde_json::from_slice(&raw[..]).unwrap()
    }
    pub fn new_json<T: Serialize>(json: &T) -> Message {
        let mut msg = Message::empty();
        msg.push_json(json);
        msg
    }
    */
}
