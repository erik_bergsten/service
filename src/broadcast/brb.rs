use crate::message::Message;
use crate::dispatch::Dispatch;
use crate::id;
use crate::vessel::Service;
use crate::timer::TimerHandler;
use crate::broadcast::{BroadcastHandler, Broadcast};
use crate::network::{NetworkHandler, Network};

use std::cell::{RefCell, Cell};
use std::collections::{HashSet};

pub const INIT: u8 = 0;
pub const ECHO: u8 = 1;
pub const READY: u8 = 2;

const MSG_CONSTRUCTOR: u8 = 0;
const ACK_CONSTRUCTOR: u8 = 1;

#[derive(Debug, Clone, PartialEq, Eq)]
pub enum BRBMessage {
    MSG(Message, u8, u16, u32),
    ACK(Message, u8, u16, u32),
}

use BRBMessage::*;

impl BRBMessage {
    fn encode(bmsg: BRBMessage) -> Message {
        match bmsg {
            MSG(mut m, p, j, s) => {
                m.push_u8(p);
                m.push_u16(j);
                m.push_u32(s);
                m.push_u8(MSG_CONSTRUCTOR);
                m
            },
            ACK(mut m, p, j, s) => {
                m.push_u8(p);
                m.push_u16(j);
                m.push_u32(s);
                m.push_u8(ACK_CONSTRUCTOR);
                m
            }
        }

    }
    pub fn to_network(bmsg: BRBMessage) -> Message {
        let mut msg = BRBMessage::encode(bmsg);
        msg
    }
    fn decode(mut msg: Message) -> Option<BRBMessage> {
        let constructor = msg.pop_u8()?;
        let s = msg.pop_u32()?;
        let j = msg.pop_u16()?;
        let p = msg.pop_u8()?;
        match constructor {
            0 => Some(MSG(msg, p, j, s)),
            1 => Some(ACK(msg, p, j, s)),
            _ => None
        }
    }
}

pub struct Entry {
    msg: Message,
    id: u16,
    seq: u32,
    delivered: bool,
    initialized: HashSet<u16>,
    echoed: HashSet<u16>,
    ready: HashSet<u16>
}

impl Entry {
    fn new(msg: Message, id: u16, seq: u32) -> Entry {
        Entry {
            msg,
            id,
            seq,
            delivered: false,
            initialized: HashSet::new(),
            echoed: HashSet::new(),
            ready: HashSet::new()
        }
    }
    fn update(&mut self, p: u8, k: u16) {
        match p {
            INIT => {
                self.initialized.insert(k);
                ()
            },
            ECHO => {
                self.echoed.insert(k);
                ()
            },
            READY => {
                self.ready.insert(k);
                ()
            },
            _ => ()
        }
    }
}

struct Buffer(Vec<Entry>);
impl Buffer {
    fn new() -> Buffer {
        Buffer(Vec::new())
    }
    pub fn update(&mut self, msg: Message, p: u8, j: u16, s: u32, k: u16) {
        let mut new = true;
        for entry in self.0.iter_mut() {
            if entry.msg == msg && entry.id == j && entry.seq == s {
                // exists in buffer already!
                new = false;
                entry.update(p, k);
            }
        }
        if new {
            let mut entry = Entry::new(msg, j, s);
            entry.update(p, k);
            self.0.push(entry);
        }
    }
    pub fn append(&mut self, other: &mut Buffer) {
        self.0.append(&mut other.0);
    }
}
pub struct BRBBroadcast<'a> {
    // interface components
    dispatch: Dispatch<&'a dyn BroadcastHandler>,
    network: &'a dyn Network,
    // constants
    id: u16,
    n: usize,
    t: usize,
    // mutable internal state

    // sequence number
    seq: Cell<u32>,
    // open messages
    buffer: RefCell<Buffer>,
    //new outgoing messages
    added: RefCell<Buffer>
}

impl Service for BRBBroadcast<'_> {
    fn id(&self) -> u16 {
        id::BROADCAST
    }
    fn name(&self) -> &str {
        "BRBBroadcast"
    }
}

impl<'a> BRBBroadcast<'a> {
    pub fn new(network: &'a dyn Network, id: u16, n: usize, t: usize) -> BRBBroadcast<'a> {
        BRBBroadcast {
            dispatch: Dispatch::new(),
            network,
            id, n, t,
            seq: Cell::new(0),
            buffer: RefCell::new(Buffer::new()),
            added: RefCell::new(Buffer::new())
        }
    }
    pub fn register(&self, service: &'a dyn BroadcastHandler) {
        self.dispatch.register(service.id(), service);
    }
    fn deliver(&self, from: u16, seq: u32, mut msg: Message) {
        let service_id = msg.pop_u16().unwrap();
        match self.dispatch.get(&service_id) {
            Some(service) => {
                service.on_broadcast(msg, from, seq);
            }
            None => {
                println!("[Network]: Invalid service id: {}", service_id);
            }
        }
    }
    fn add(&self, msg: Message, p: u8, j: u16, s: u32, k: u16) {
        let mut added = self.added.borrow_mut();
        added.update(msg, p, j, s, k);
    }
    fn update(&self, msg: Message, p: u8, j: u16, s: u32, k: u16) {
        let mut buffer = self.buffer.borrow_mut();
        buffer.update(msg, p, j, s, k);
    }
    fn ur_broadcast(&self, msg: Message, p: u8, j: u16, s: u32, received: &HashSet<u16>) {
        //println!("Broadcasting to {} guys!", self.n - received.len());
        let raw_msg = MSG(msg, p, j, s);
        let encoded_msg = BRBMessage::to_network(raw_msg);
        for i in 0..(self.n as u16) {
            if !received.contains(&i) {
                self.network.send(i, encoded_msg.clone(), self.id());
            }
        }
    }
}
impl NetworkHandler for BRBBroadcast<'_> {
    fn on_message(&self, from: u16, msg: Message) {
        match BRBMessage::decode(msg) {
            Some(MSG(m, p, j, s)) => {
                if p == INIT && j != from {
                    // do nothing, invalid message
                    //println!("Bad init received!");
                } else {
                    self.update(m.clone(), p, j, s, from);
                    // send ACK!
                    let ack = ACK(m, p, j, s);
                    let encoded = BRBMessage::to_network(ack);
                    self.network.send(from, encoded, self.id());
                }
            },
            Some(ACK(m, p, j, s)) => {
                self.update(m.clone(), p, j, s, from);
            },
            None => {
                println!("Received un-parseable message.");
            }
        }
    }
}

impl TimerHandler for BRBBroadcast<'_> {
    fn on_tick(&self, _tick: usize, _event: u16) {
        let mut buffer = self.buffer.borrow_mut();
        {
            let mut added = self.added.borrow_mut();
            buffer.append(&mut added);
        }
        //println!("[Broadcast]: Open count: {}", buffer.0.len());
        let open = buffer.0.len();
        for entry in buffer.0.iter_mut() {
            if entry.ready.len() > 2*self.t && !entry.delivered{
                // we are ready to deliver
                //println!("Open broadcasts: {}", open);
                self.deliver(entry.id, entry.seq, entry.msg.clone());
                entry.delivered = true;
            }
            if entry.ready.len() > self.t || entry.echoed.len() > (self.n+self.t)/2 {
                // we must send out readies!
                self.ur_broadcast(entry.msg.clone(),READY,entry.id,entry.seq, &entry.ready);
            }else{
                // we must send out echoes and maybe init!
                if entry.initialized.contains(&entry.id) {
                    self.ur_broadcast(entry.msg.clone(),ECHO,entry.id,entry.seq, &entry.echoed);
                }
                if entry.id == self.id {
                    // we must keep sending INIT until we get echoes
                    self.ur_broadcast(entry.msg.clone(),INIT,entry.id,entry.seq, &entry.initialized);

                }
            }
        }
    }
}
impl Broadcast for BRBBroadcast<'_> {
    fn init(&self, mut msg: Message, service_id: u16) {
        let seq = self.seq.get();
        self.seq.set(seq + 1);
        msg.push_u16(service_id);
        self.add(msg, INIT, self.id, seq, self.id);
    }
}

