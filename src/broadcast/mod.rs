use crate::message::Message;
use crate::vessel::Service;

pub mod best_effort;
pub use best_effort::BEBroadcast;
pub mod no_duplicity;
pub use no_duplicity::NDBroadcast;
pub mod brb;
pub use brb::BRBBroadcast;

pub trait BroadcastHandler: Service {
    fn on_broadcast(&self, msg: Message, from: u16, seq: u32);
}

// only function provided by broadcasts: init - which begins a broadcast
// should maybe be called BROADCAST or do_broadcast or something but in
// most articles it is referred to as INIT so whatever
pub trait Broadcast: Service {
    fn init(&self, msg: Message, service_id: u16);
}
