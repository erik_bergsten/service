use crate::broadcast::{Broadcast, BroadcastHandler};
use crate::vessel::{Service, Vessel};
use crate::dispatch::Dispatch;
use crate::message::Message;
use crate::network::{NetworkHandler, Network};
use crate::id;
use crate::timer::{TimerHandler};
use std::cell::{Cell, RefCell};
use std::collections::HashSet;

const MSG: u8 = 0;
const ACK: u8 = 1;

struct Entry {
    msg: Message,
    id: u16,
    seq: u32,
    received: HashSet<u16>,
    delivered: bool
}
impl Entry {
    fn new(msg: Message, id: u16, seq: u32, from: u16) -> Entry {
        let mut received = HashSet::new();
        received.insert(from);
        Entry {
            msg,
            id,
            seq,
            received,
            delivered: false
        }
    }
}
pub fn encode(msg: &Message, constructor: u8, id: u16, seq: u32) -> Message {
    let mut out = msg.clone();
    out.push_u8(constructor);
    out.push_u16(id);
    out.push_u32(seq);
    out
}
pub fn decode(msg: &mut Message) -> (u8, u16, u32) {
    let seq = msg.pop_u32().unwrap();
    let id = msg.pop_u16().unwrap();
    let constructor = msg.pop_u8().unwrap();
    (constructor, id, seq)

}

pub struct BEBroadcast<'a> {
    dispatch: Dispatch<&'a dyn BroadcastHandler>,
    network: &'a dyn Network,
    vessel: &'a Vessel<'a>,
    seq: Cell<u32>,
    buffer: RefCell<Vec<Entry>>,
    added: RefCell<Vec<Entry>>
}
impl Service for BEBroadcast<'_> {
    fn id(&self) -> u16 {
        id::BROADCAST
    }
}
impl<'a> BEBroadcast<'a> {
    pub fn new(vessel: &'a Vessel<'a>, network: &'a dyn Network) -> BEBroadcast<'a> {
        BEBroadcast {
            dispatch: Dispatch::new(),
            vessel,
            network,
            seq: Cell::new(0),
            buffer: RefCell::new(Vec::new()),
            added: RefCell::new(Vec::new())
        }
    }
    pub fn register(&self, service: &'a dyn BroadcastHandler) {
        self.dispatch.register(service.id(), service);
    }
    fn ur_broadcast(&self, msg: &Message, id: u16, seq: u32, received: &HashSet<u16>){
        let out = encode(msg, MSG, id, seq);
        for i in 0..self.network.n() {
            if !received.contains(&i) {
                self.network.send(i, out.clone(), self.id());
            }
        }
    }
    fn tick(&self, entry: &mut Entry) {
        if !entry.delivered {
            self.deliver(entry.msg.clone(), entry.id, entry.seq);
            entry.delivered = true;
        }
        if entry.id == self.vessel.id {
            self.ur_broadcast(&entry.msg, entry.id, entry.seq, &entry.received);
        }
    }
    fn deliver(&self, mut msg: Message, from: u16, seq: u32) {
        let service_id = msg.pop_u16().unwrap();
        match self.dispatch.get(&service_id) {
            Some(service) => {
                service.on_broadcast(msg, from, seq);
            }
            None => {
                println!("[BEBroadcast]: Invalid service id: {}", service_id);
            }
        }
    }
    fn update(&self, msg: Message, id: u16, seq: u32, from: u16) {
        let mut buffer = self.buffer.borrow_mut();
        let mut new = true;
        for entry in buffer.iter_mut() {
            if entry.msg == msg && entry.id == id && entry.seq == seq {
                entry.received.insert(from);
                new = false
            }
        }
        if new {
            let entry = Entry::new(msg, id, seq, from);
            buffer.push(entry);
        }
    }
}
impl Broadcast for BEBroadcast<'_> {
    fn init(&self, mut msg: Message, service_id: u16){
        let seq = self.seq.get();
        self.seq.set(seq+1);
        msg.push_u16(service_id);
        let entry = Entry::new(msg, self.vessel.id, seq, self.vessel.id);
        self.added.borrow_mut().push(entry);
    }
}
impl NetworkHandler for BEBroadcast<'_> {
    fn on_message(&self, from: u16, mut msg: Message) {
        let (constructor, id, seq) = decode(&mut msg);
        if constructor != ACK {
            // send ack
            let out = encode(&msg, ACK, id, seq);
            self.network.send(from, out, self.id());
        }
        self.update(msg, id, seq, from);
    }
}
impl TimerHandler for BEBroadcast<'_> {
    fn on_tick(&self, _tick: usize, _event: u16) {
        let mut buffer = self.buffer.borrow_mut();
        {
            let mut added = self.added.borrow_mut();
            buffer.append(&mut added);
        }

        for mut entry in buffer.iter_mut() {
            self.tick(&mut entry);
        }
    }
}


