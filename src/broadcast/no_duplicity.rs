use crate::broadcast::{Broadcast, BroadcastHandler};
use crate::vessel::{Service, Vessel};
use crate::dispatch::Dispatch;
use crate::message::Message;
use crate::network::{NetworkHandler, Network};
use crate::id;
use crate::timer::{TimerHandler};
use std::cell::{Cell, RefCell};
use std::collections::{HashMap, HashSet};

const MSG: u8 = 0;
const ACK: u8 = 1;

struct Entry {
    msg: Message,
    id: u16,
    seq: u32,
    received: HashSet<u16>,
    accepted: HashSet<u16>,
    delivered: bool
}
impl Entry {
    fn new(msg: Message, j: u16, seq: u32) -> Entry {
        let mut received = HashSet::new();
        let mut accepted = HashSet::new();
        Entry {
            msg,
            id: j,
            seq,
            received,
            accepted,
            delivered: false
        }
    }
}
pub fn encode(msg: &Message, constructor: u8, id: u16, seq: u32) -> Message {
    let mut out = msg.clone();
    out.push_u8(constructor);
    out.push_u16(id);
    out.push_u32(seq);
    out
}
pub fn decode(msg: &mut Message) -> (u8, u16, u32) {
    let seq = msg.pop_u32().unwrap();
    let id = msg.pop_u16().unwrap();
    let constructor = msg.pop_u8().unwrap();
    (constructor, id, seq)

}
pub struct NDBroadcast<'a> {
    dispatch: Dispatch<&'a dyn BroadcastHandler>,
    network: &'a dyn Network,
    vessel: &'a Vessel<'a>,
    t: u16,
    seq: Cell<u32>,
    buffer: RefCell<Vec<Entry>>,
    added: RefCell<Vec<Entry>>,
    lastDelivered: RefCell<HashMap<u16, u32>>,
    lastReceived: RefCell<HashMap<u16, u32>>
}
impl Service for NDBroadcast<'_> {
    fn id(&self) -> u16 {
        id::BROADCAST
    }
}
impl<'a> NDBroadcast<'a> {
    pub fn new(vessel: &'a Vessel<'a>, network: &'a dyn Network, t: u16) -> NDBroadcast<'a> {
        let mut lastReceived = HashMap::new();
        let mut lastDelivered = HashMap::new();
        for i in 0..network.n() {
            lastReceived.insert(i, 0);
            lastDelivered.insert(i, 0);
        }

        NDBroadcast {
            dispatch: Dispatch::new(),
            vessel,
            network,
            t,
            seq: Cell::new(1),
            buffer: RefCell::new(Vec::new()),
            added: RefCell::new(Vec::new()),
            lastDelivered: RefCell::new(lastDelivered),
            lastReceived: RefCell::new(lastReceived)
        }
    }
    pub fn register(&self, service: &'a dyn BroadcastHandler) {
        self.dispatch.register(service.id(), service);
    }
    fn ur_broadcast(&self, msg: &Message, id: u16, seq: u32, received: &HashSet<u16>){
        let out = encode(msg, MSG, id, seq);
        for i in 0..self.network.n() {
            if !received.contains(&i) {
                self.network.send(i, out.clone(), self.id());
            }
        }
    }
    fn broadcast_entry(&self, entry: &Entry){
        let out = encode(&entry.msg, MSG, entry.id, entry.seq);
        for i in 0..self.network.n() {
            if !entry.received.contains(&i) {
                self.network.send(i, out.clone(), self.id());
            }
        }
    }
    fn tick(&self, entry: &mut Entry) {
        if !entry.delivered && entry.accepted.len() > ((self.network.n()+self.t)/2) as usize  {
            self.deliver(entry.msg.clone(), entry.id, entry.seq);
            entry.delivered = true;
        }
        if entry.accepted.contains(&entry.id) && entry.received.len() < self.network.n() as usize {
            //println!("Rebroadcasting ({},{})!", entry.id, entry.seq);
            // if we have received it from the original sender
            // but not received it from everyone, then we must broadcats again
            //self.ur_broadcast(&entry.msg, entry.id, entry.seq, &entry.received);
            self.broadcast_entry(&entry);
        } else {
            //println!("Not rebroadcasting ({},{})!", entry.id, entry.seq);
        }
    }
    fn deliver(&self, mut msg: Message, from: u16, seq: u32) {
        let service_id = msg.pop_u16().unwrap();
        match self.dispatch.get(&service_id) {
            Some(service) => {
                service.on_broadcast(msg, from, seq);
            }
            None => {
                println!("[NDBroadcast]: Invalid service id: {}", service_id);
            }
        }
    }
    fn accepted(&self, msg: Message, id: u16, seq: u32, from: u16) {
        let mut buffer = self.buffer.borrow_mut();
        let mut new = true;
        for mut entry in buffer.iter_mut() {
            if entry.msg == msg && entry.id == id && entry.seq == seq {
                entry.accepted.insert(from);
                if entry.id == from {
                    // we just got the init - do a rebroadcast instantly
                    //self.ur_broadcast(&entry.msg,entry.id,entry.seq,&entry.received);
                }
                new = false;
                //self.tick(&mut entry);
            }
        }
        if new {
            let mut entry = Entry::new(msg, id, seq);
            if from == id {
                let mut lastReceived = self.lastReceived.borrow_mut();
                // the message is an init!
                let lastSeq = *lastReceived.get(&id).unwrap();
                if (lastSeq+1) == seq {
                    lastReceived.insert(id, seq);
                    entry.received.insert(self.vessel.id);
                    entry.accepted.insert(self.vessel.id);
                } else {
                    println!("Someone is trying to sneak one in!");
                    return;
                }
            }
            //self.tick(&mut entry);
            entry.accepted.insert(from);
            buffer.push(entry);
        }
    }
    fn acknowledged(&self, msg: Message, id: u16, seq: u32, from: u16) {
        let mut buffer = self.buffer.borrow_mut();
        for entry in buffer.iter_mut() {
            if entry.msg == msg && entry.id == id && entry.seq == seq {
                entry.received.insert(from);
            }
        }
    }
}
impl Broadcast for NDBroadcast<'_> {
    fn init(&self, mut msg: Message, service_id: u16){
        let seq = self.seq.get();
        self.seq.set(seq+1);
        msg.push_u16(service_id);
        let mut entry = Entry::new(msg, self.vessel.id, seq);
        entry.accepted.insert(self.vessel.id);
        entry.received.insert(self.vessel.id);
        //self.tick(&entry);
        self.added.borrow_mut().push(entry);
    }
}
impl NetworkHandler for NDBroadcast<'_> {
    fn on_message(&self, from: u16, mut msg: Message) {
        let (constructor, id, seq) = decode(&mut msg);
        if constructor == MSG {
            //println!("THX for message {} from {}", seq, from);
            //println!("Got message ({},{}) from {}", id, seq, from);
            let response = encode(&msg, ACK, id, seq);
            self.network.send(from, response, self.id());

            self.accepted(msg, id, seq, from);
        }else {
            self.acknowledged(msg, id, seq, from);
            //println!("Got ack for ({},{}) from {}", id, seq, from);
        }
        /*
        if constructor != ACK {
            // send ack
            let out = encode(&msg, ACK, id, seq);
            self.network.send(from, out, self.id());
            self.accept(msg, id, seq, from);
        } else {
            self.acknowledge(msg, id, seq, from);
        }
        */
    }
}
impl TimerHandler for NDBroadcast<'_> {
    fn on_tick(&self, _tick: usize, _event: u16) {
        let mut buffer = self.buffer.borrow_mut();
        {
            let mut added = self.added.borrow_mut();
            buffer.append(&mut added);
        }

        for mut entry in buffer.iter_mut() {
            self.tick(&mut entry);
        }
    }
}




