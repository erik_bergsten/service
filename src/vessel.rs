use crate::dispatch::Dispatch;
use crate::message::Message;
use std::sync::mpsc::{channel, Receiver, Sender};


/// The base trait of any service.
pub trait Service {
    /// Unique identifier of this service.
    fn id(&self) -> u16;
    /// The name of the service.
    fn name(&self) -> &str {
        "Anonymous"
    }
    /// Utility function for services.
    fn show(&self) {
        println!("Service [{}] with id #{}.", self.name(), self.id());
    }
}

/// Callback trait for services which listen to incoming events.
pub trait EventHandler: Service {
    /// Events usually come with a source and a message.
    fn on_event(&self, from: u16, msg: Message);
}

/// The base structure of the framework.
pub struct Vessel<'a> {
    /// A vessel identifier - not to identify it amongst services but rather
    /// amongst nodes in the network.
    pub id: u16,
    /// The private, read-end of the main event channel.
    event_rx: Receiver<(u16, Message)>,
    /// The public, write-end of the main event channel.
    pub event_tx: Sender<(u16, Message)>,
    /// The central dispatch. Usually only the network service and some timer
    /// or local io service will be registered here.
    dispatch: Dispatch<&'a dyn EventHandler>,
}

impl<'a> Vessel<'a> {
    /// Create a new vessel.
    pub fn new(id: u16) -> Vessel<'a> {
        let (event_tx, event_rx) = channel();
        Vessel {
            id,
            event_rx,
            event_tx,
            dispatch: Dispatch::new(),
        }
    }
    /// Register an event handler.
    pub fn register(&self, service: &'a dyn EventHandler) {
        self.dispatch.register(service.id(), service);
    }

    /// Add an event to the main queue. You can also copy the sender
    /// (which is public) if you need to send it to another thread.
    pub fn send(&self, from: u16, msg: Message) {
        self.event_tx.send((from, msg)).unwrap();
    }

    pub fn handle_event(&self, from: u16, mut message: Message) {
        // TODO: error check message.pop_u16()
        let service_id = message.pop_u16().unwrap();
        match self.dispatch.get(&service_id) {
            Some(service) => {
                service.on_event(from, message);
            }
            None => {
                println!("[Vessel]: Invalid service id: {}", service_id);
            }
        }
    }
    pub fn tick(&self) {
        let (from, message) = self.event_rx.recv().unwrap();
        self.handle_event(from, message);
    }
    /// Start the vessel! Runs until it crashes.
    pub fn run(&self) {
        loop {
            self.tick();
        }
    }
}
