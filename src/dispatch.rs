use std::cell::RefCell;
use std::collections::HashMap;

pub struct Dispatch<T>(RefCell<HashMap<u16, T>>);
impl<T: Copy> Dispatch<T> {
    pub fn new() -> Dispatch<T> {
        Dispatch(RefCell::new(HashMap::new()))
    }
    pub fn register(&self, id: u16, service: T) {
        let mut hashmap = self.0.borrow_mut();
        hashmap.insert(id, service);
    }
    pub fn get(&self, id: &u16) -> Option<T> {
        match self.0.borrow().get(id) {
            Some(service) => Some(*service),
            None => None,
        }
    }
}
