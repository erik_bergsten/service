use crate::message::Message;
use crate::vessel::Service;

/// Callback trait for network using services.
pub trait NetworkHandler: Service {
    /// From will identify the node the message came from.
    fn on_message(&self, from: u16, msg: Message);
}

/// The trait for networks! Needs to be implemented so services which use
/// networks can!
pub trait Network {
    /// Send to one specific node.
    fn send(&self, to: u16, msg: Message, service_id: u16);
    /// Send to all nodes.
    fn broadcast(&self, msg: Message, service_id: u16);
    /// Number of hosts in network. Defined as number of hosts broadcast will
    /// attempt to send to.
    fn n(&self) -> u16;
}
