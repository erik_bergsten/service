use crate::network::{NetworkHandler, Network};
use crate::vessel::Service;
use crate::message::Message;
use std::cell::Cell;

pub struct NetworkStats<'a> {
    sent: Cell<usize>,
    received: Cell<usize>,
    service: Cell<Option<&'a dyn NetworkHandler>>,
    network: &'a dyn Network,
}

impl Service for NetworkStats<'_> {
    fn id(&self) -> u16 {
        self.service.get().unwrap().id()
    }
    fn name(&self) -> &str {
        "Network STATS"
    }
}
impl Network for NetworkStats<'_> {
    fn n(&self) -> u16 {
        self.network.n()
    }
    fn send(&self, to: u16, msg: Message, service_id: u16) {
        let sent = self.sent.get();
        self.sent.set(sent+1);
        self.network.send(to, msg, service_id);
    }
    fn broadcast(&self, msg: Message, service_id: u16) {
        let sent = self.sent.get();
        self.sent.set(sent+ (self.n() as usize) );
        self.network.broadcast(msg, service_id);
    }
}
impl NetworkHandler for NetworkStats<'_> {
    fn on_message(&self, from: u16, msg: Message) {
        let received = self.received.get();
        self.received.set(received+1);
        let service = self.service.get().unwrap();
        service.on_message(from, msg);
    }
}
impl<'a> NetworkStats<'a> {
    pub fn new(network: &'a dyn Network) -> NetworkStats<'a> {
        NetworkStats {
            sent: Cell::new(0),
            received: Cell::new(0),
            service: Cell::new(None),
            network
        }
    }
    pub fn register(&self, service: &'a dyn NetworkHandler) {
        self.service.set(Some(service));
    }
    pub fn sent(&self) -> usize {
        self.sent.get()
    }
    pub fn received(&self) -> usize {
        self.received.get()
    }
}
