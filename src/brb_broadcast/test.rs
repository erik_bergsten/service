use crate::brb_broadcast::{BRBMessage, Buffer, Entry};
use crate::message::Message;

#[test]
fn msg_encode_decode() {
    let content = Message::from_bytes(b"hello");
    let init = BRBMessage::MSG(content.clone(), 0, 1, 2);
    let init_decoded = BRBMessage::decode(BRBMessage::encode(init.clone())).unwrap();
    // TEST THAT THE VALUE IS UNCHANGED AFTER ENCODING AND DECODING IT
    assert_eq!(init, init_decoded);
    let ack = BRBMessage::ACK(content.clone(), 0, 3, 4);
    let ack_decoded = BRBMessage::decode(BRBMessage::encode(ack.clone())).unwrap();
    // TEST THAT THE VALUE IS UNCHANGED AFTER ENCODING AND DECODING IT
    assert_eq!(ack, ack_decoded);
    let mut ack_encoded = BRBMessage::encode(ack);
    ack_encoded.pop_u8().unwrap();
    ack_encoded.push_u8(123);
    // make sure it returns NONE if the contsructor is INVALID
    assert_eq!(None, BRBMessage::decode(ack_encoded));
}

#[test]
fn buffereing() {
    let mut buffer = Buffer::new();
    assert_eq!(buffer.0.len(), 0);
    let msg1 = Message::from_bytes(b"AAAA");
    let msg2 = Message::from_bytes(b"BBBB");

    // test that length will change after updating with new INIT
    buffer.update(msg1.clone(), 0, 0, 0, 0);
    assert_eq!(buffer.0.len(), 1);
    // test that length wnot change after same init
    buffer.update(msg1.clone(), 0, 0, 0, 0);
    assert_eq!(buffer.0.len(), 1);

    assert_eq!(buffer.0[0].initialized.contains(&0), true);
    assert_eq!(buffer.0[0].echoed.len(), 0);
    buffer.update(msg1.clone(), 1, 0, 0, 0);
    assert_eq!(buffer.0[0].echoed.len(), 1);
    buffer.update(msg1.clone(), 1, 0, 0, 0);
    assert_eq!(buffer.0[0].echoed.len(), 1);
    buffer.update(msg1.clone(), 1, 0, 0, 1);
    assert_eq!(buffer.0[0].echoed.len(), 2);
    buffer.update(msg1.clone(), 1, 0, 0, 2);
    assert_eq!(buffer.0[0].echoed.len(), 3);


}

