use crate::broadcast::{BroadcastHandler, Broadcast};
use crate::dispatch::Dispatch;
use crate::id;
use crate::message::Message;
use crate::timer::TimerHandler;
use crate::network::{NetworkHandler, Network};
use crate::vessel::Service;

use std::cell::Cell;
use std::cell::RefCell;
use std::collections::{HashMap, HashSet};

use serde::{Deserialize,Serialize};

// the messages used by the algorithm
#[derive(Debug,Serialize,Deserialize)]
pub enum NDMessage {
    MSG(Message, u16, u32),
    ACK(u16, u32),
}
use NDMessage::*;

// BUFFER ENTRY
#[derive(Debug)]
struct Entry {
    msg: Message,
    id: u16,
    seq: u32,
    delivered: bool,
    initialized: bool,
    received_by: HashSet<u16>,
}
impl Entry {
    fn new(id: u16, seq: u32, msg: Message, from: u16) -> Entry {
        let mut received_by = HashSet::new();
        received_by.insert(from);
        Entry {
            msg,
            id,
            seq,
            initialized: id == from,
            delivered: false,
            received_by
        }
    }
}
pub struct NDBroadcast<'a> {
    dispatch: Dispatch<&'a dyn BroadcastHandler>,
    id: u16, //node id
    n: usize,  //number of nodes
    t: usize,  //maximum number of faulty nodes
    network: &'a dyn Network,
    // mutable internal state
    seq: Cell<u32>,
    buffer: RefCell<Vec<Entry>>,
    added: RefCell<Vec<Entry>>
}

impl Service for NDBroadcast<'_> {
    fn id(&self) -> u16 {
        id::BROADCAST
    }
    fn name(&self) -> &str {
        "NDBroadcast"
    }
}
impl NetworkHandler for NDBroadcast<'_> {
    fn on_message(&self, from: u16, mut msg: Message) {
        //println!("NDBroadcast got message from {}: {}", from, msg.as_str());
        let nd_msg: NDMessage = msg.pop_json();
        println!("NDBroadcast got {:?} from {}", nd_msg, from);
    }
}
impl TimerHandler for NDBroadcast<'_> {
    fn on_tick(&self, _tick: usize, event: u16) {
        let mut buffer = self.buffer.borrow_mut();
        for entry in buffer.iter_mut() {
            if entry.received_by.len() > ((self.n+self.t) / 2) {
                // deliver!
                println!("Ready to deliver: {} from {}",entry.msg.as_str(),entry.id);
                entry.delivered = true;
            }
            if entry.id == self.id || entry.initialized {
                self.ur_broadcast(entry.msg.clone(), entry.id, entry.seq, &entry.received_by);
            }
        }
    }
}
impl<'a> NDBroadcast<'a> {
    pub fn new(network: &'a dyn Network, id: u16, n: usize, t: usize) -> NDBroadcast<'a> {
        NDBroadcast {
            dispatch: Dispatch::new(),
            seq: Cell::new(0),
            network,
            id,
            n,
            t,
            buffer: RefCell::new(Vec::new()),
            added: RefCell::new(Vec::new())
        }
    }
    pub fn add(&self, message: Message) {
        let seq = self.seq.get();
        self.seq.set(seq+1);
        let entry = Entry::new(self.id, seq, message, self.id);
        self.added.borrow_mut().push(entry);
    }
    pub fn update(&self, message: Message, j: u16, s: u32, k: u16) {
        let mut buffer = self.buffer.borrow_mut();
        let mut new = true;
        for entry in buffer.iter_mut() {
            if entry.msg == message && entry.id == j && entry.seq == s {
                new = false;
                entry.received_by.insert(k);
                if k == j {
                    entry.initialized = true;
                }
                return
            }
        }
        if new {
            let entry = Entry::new(j, s, message, k);
            buffer.push(entry);
        }

    }
    pub fn register(&self, service: &'a dyn BroadcastHandler) {
        self.dispatch.register(service.id(), service);
    }
    fn deliver(&self, from: u16, mut msg: Message) {
        let service_id = msg.pop_u16().unwrap();
        match self.dispatch.get(&service_id) {
            Some(service) => {
                service.on_broadcast(from, msg);
            }
            None => {
                println!("[NDBroadcast]: Invalid service id: {}", service_id);
            }
        }
    }
    fn ur_broadcast(&self, msg: Message, j: u16, s: u32, received: &HashSet<u16>) {
        let raw_msg = MSG(msg, j, s);
        let encoded_msg = Message::new_json(&raw_msg);
        for i in 0..(self.n as u16) {
            if !received.contains(&i) {
                self.network.send(i, encoded_msg.clone());
            }
        }
    }
}
impl Broadcast for NDBroadcast<'_> {
    fn init(&self, msg: Message) {
        self.add(msg);
    }
}
