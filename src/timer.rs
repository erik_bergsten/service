use crate::id;
use crate::message::Message;
use crate::vessel::{EventHandler, Service, Vessel};
use std::cell::{Cell, RefCell};
use std::sync::mpsc::Sender;
use std::thread;
use std::time::Duration;


pub trait TimerHandler: Service {
    fn on_tick(&self, tick: usize, event: u16);
}

struct Subscriber<'a> {
    service: &'a dyn TimerHandler,
    event: u16,
    interval: u64,
    time: u64,
    ticks: Cell<usize>
}
pub struct Timer<'a> {
    subscribers: RefCell<Vec<Subscriber<'a>>>,
    timeout: u64,
}
impl Service for Timer<'_> {
    fn id(&self) -> u16 {
        id::TIMER
    }
    fn name(&self) -> &str {
        "Timer"
    }
}
impl EventHandler for Timer<'_> {
    fn on_event(&self, from: u16, _msg: Message) {
        for subscriber in self.subscribers.borrow_mut().iter_mut() {
            subscriber.time += self.timeout;
            while subscriber.time > subscriber.interval {
                let tick = subscriber.ticks.get();
                subscriber.ticks.set(tick+1);
                subscriber.service.on_tick(tick, subscriber.event);
                subscriber.time -= subscriber.interval;
            }
        }
    }
}

impl<'a> Timer<'a> {
    pub fn new(vessel: &Vessel, timeout: u64) -> Timer<'a> {
        let mut msg = Message::empty();
        let duration = Duration::from_millis(timeout);
        let event_tx = vessel.event_tx.clone();
        msg.push_u16(id::TIMER);
        thread::spawn(move || loop {
            event_tx.send((0, msg.clone())).unwrap();
            thread::sleep(duration);
        });
        Timer {
            subscribers: RefCell::new(Vec::new()),
            timeout,
        }
    }
    pub fn register(&self, service: &'a dyn TimerHandler, event: u16, interval: u64) {
        let subscriber = Subscriber {
            service,
            event,
            interval,
            time: 0,
            ticks: Cell::new(0)
        };
        self.subscribers.borrow_mut().push(subscriber);
    }
}
