use crate::broadcast::{Broadcast, NDBroadcast, BroadcastHandler};
use crate::vessel::{Service, Vessel};
use crate::network::Network;//, Vessel};
use crate::dispatch::Dispatch;
use crate::message::Message;
use crate::id;
use crate::timer::TimerHandler;
use crate::consensus::{BinaryConsensus, BinaryConsensusSeeker};
use std::cell::{Cell, RefCell};
use std::collections::{HashMap, HashSet};

struct Proposition {
    prop: Message,
    estimates: HashMap<(u16, u16), bool>,
    round: u16,
    decided: HashSet<u16>
}

impl Proposition {
    fn new(prop: Message) -> Proposition {
        let estimates = HashMap::new();
        Proposition {
            prop,
            estimates,
            round: 1,
            decided: HashSet::new()
        }
    }
    fn estimate_count(&self, r: u16) -> (u16, u16, u16) {
        let mut count = 0;
        let mut t = 0;
        let mut f = 0;
        for ((_id, round), estimate) in self.estimates.iter() {
            if r == *round {
                count += 1;
                if *estimate {
                    t += 1;
                } else {
                    f += 1;
                }
            }
        }
        (count, t, f)
    }
}

pub struct BBConsensus<'a> {
    vessel: &'a Vessel<'a>,
    network: &'a dyn Network,
    broadcast: &'a NDBroadcast<'a>,
    t: u16,
    buffer: RefCell<Vec<Proposition>>,
    added: RefCell<Vec<Proposition>>,
    dispatch: Dispatch<&'a dyn BinaryConsensusSeeker>
}

fn encode(prop: Message, constructor: u8, round: u16, estimate: bool) -> Message {
    let mut msg = prop;
    msg.push_u8(constructor);
    msg.push_u16(round);
    msg.push_u8(estimate as u8);
    return msg;
}
fn decode(msg: &mut Message) -> (u8, u16, bool) {
    let estimate = msg.pop_u8().unwrap() != 0;
    let round = msg.pop_u16().unwrap();
    let constructor = msg.pop_u8().unwrap();
    return (constructor, round, estimate)
}
impl<'a> BBConsensus<'a> {
    pub fn new(vessel: &'a Vessel<'a>, network: &'a dyn Network, broadcast: &'a NDBroadcast<'a>, t: u16) -> BBConsensus<'a> {
        BBConsensus {
            vessel,
            network,
            broadcast,
            t,
            buffer: RefCell::new(Vec::new()),
            added: RefCell::new(Vec::new()),
            dispatch: Dispatch::new()
        }
    }
    pub fn register(&self, service: &'a dyn BinaryConsensusSeeker) {
        //println!("Registering! {}", service.id());
        self.dispatch.register(service.id(), service);
    }
    fn decide(&self, mut proposition: Message, decision: bool) {
        let service_id = proposition.pop_u16().unwrap();
        match self.dispatch.get(&service_id) {
            Some(service) => {
                service.on_consensus(proposition, decision);
            },
            None => {
                println!("[Binary Consensus]: Invalid service id: {}", service_id);
            }
        }



    }

    fn on_vote(&self, proposition: &mut Proposition, c: u8, r: u16, estimate: bool, from: u16) {
        if c == 0 {
            proposition.estimates.insert( (from, r), estimate );
        } else {
            proposition.decided.insert(from);
            for i in 0..r {
                proposition.estimates.insert( (from, i), estimate );
            }
        }
        if proposition.decided.contains(&self.vessel.id) {
            // we are done!
            return;
        }
        let (count, t, f) = proposition.estimate_count(proposition.round);
        println!("Vote received! round: {} = ({},{},{})", proposition.round,
                    count, t, f);
        let quorum = (self.network.n() + self.t) / 2;
        if count > quorum {
            let mut my_estimate = true; // default to true
            if f > t {
                // more noes than yesses
                my_estimate = false;
                if f > quorum {
                    // a quorum says no! we go no!
                    let msg = encode(proposition.prop.clone(), 1, proposition.round, false);
                    self.broadcast.init(msg, self.id());
                    self.decide(proposition.prop.clone(), my_estimate);
                    proposition.decided.insert(self.vessel.id);
                    println!("Decided {:?} is {} on round {}.", proposition.prop, my_estimate, proposition.round);
                    return;
                }
            }else if t > quorum {
                let msg = encode(proposition.prop.clone(), 1, proposition.round, true);
                self.broadcast.init(msg, self.id());
                self.decide(proposition.prop.clone(), my_estimate);
                proposition.decided.insert(self.vessel.id);
                    println!("Decided {:?} is {} on round {}.", proposition.prop, my_estimate, proposition.round);
                return;
            }
            proposition.round += 1;
            let msg = encode(proposition.prop.clone(),0, proposition.round,my_estimate);
            self.broadcast.init(msg, self.id());
        }
    }
}

impl Service for BBConsensus<'_> {
    fn id(&self) -> u16 {
        id::CONSENSUS
    }
    fn name(&self) -> &str {
        "Byzantine Binary Consensus"
    }
}

impl BroadcastHandler for BBConsensus<'_> {
    fn on_broadcast(&self, mut msg: Message, from: u16, _seq: u32) {
        let (c, r, est) = decode(&mut msg);
        /*
        if c == 0 {
            println!("Got proposition: {:?},{},{} from {}", msg, r, est, from);
        } else {
            println!("Got decision: {:?},{},{} from {}", msg, r, est, from);
        }
        */

        let mut buffer = self.buffer.borrow_mut();
        for mut proposition in buffer.iter_mut() {
            if proposition.prop == msg {
                self.on_vote(&mut proposition, c, r, est, from);
                return;
            }
        }
        // new proposition received before we iitialized it
        //println!("Funny new prop!");
        let mut proposition = Proposition::new(msg.clone());
        if c == 0 {
            proposition.estimates.insert((from, r), est);
        } else {
            for i in 0..r {
                proposition.estimates.insert((from, i), est);
            }
        }
        buffer.push(proposition);
    }
}

impl BinaryConsensus for BBConsensus<'_> {
    fn propose(&self, mut proposition: Message, estimate: bool, service_id: u16) {
        proposition.push_u16(service_id);
        let msg = encode(proposition, 0, 1, estimate);
        self.broadcast.init(msg, self.id());
    }
}

