use crate::message::Message;
use crate::vessel::Service;

mod binary_byzantine;
pub use binary_byzantine::BBConsensus;

pub trait BinaryConsensus {
    fn propose(&self, proposition: Message, estimate: bool, service_id: u16);
}
pub trait BinaryConsensusSeeker : Service {
    fn on_consensus(&self, proposition: Message, decision: bool);
}
