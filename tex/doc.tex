\documentclass{article}
\usepackage{graphicx}
\usepackage{xcolor}
\usepackage{listings}
\usepackage{caption}
\usepackage{courier}

\lstnewenvironment{pseudo}[1][] {
  \lstset{
    mathescape=true,
    frame=tB,
    numbers=left,
    basicstyle=\small\ttfamily,
    keywordstyle=\color{black}\bfseries\em,
    keywords={,operation,on,if,end,then,and,var,struct, upon,for,forever,procedure,not,in},
    xleftmargin=.04\textwidth,
    #1
  }
}
{}


\begin{document}

\title{Rust Implementaton of Byzantine Reliable Broadcasts. (DAT085)}
\author{Erik Bergsten \\
        eribergs@student.chalmers.se \\
        supervisor: Elad Schiller
        }
\maketitle


\section{Purpose}


The goal of this project is to implement one or some of the algorithms from
chapter 4 of the book \ref{test}. The first algorithm is called a
no-duplicity broadcast and it is a byzantine fault-tolerant algorithm for
broadcasting which guarantees simply that no two different messages will
be delivered as the same message by any two processes in the system.

The second algorithm builds on top of this to create a complete Byzantine Reliable
Broadcast (BRB) algorithm
which fulfills the conditions of a reliable broadcast and has the properties of
a uniform reliable broadcast with one
small caveat: faulty processes cannot be required to
complete a broadcast in the byzantine fault model since their byzantine
behaviour might include not delivering a message that they should have delivered.

They both use the Byzantine Asynchcronous
Message Passing ($BAMP[t<n/3]$) fault model where $t$, the number of faulty
processes, is less than a third of $n$, the number of processes.

The book also describes a final algorithm, a more message efficient broadcast
for the fault model $BAMP[t<n/5]$.

I will implement (some of) the algorithms and any necesseary architecture in rust and
the evaluation will be done in the network simulation tool Mininet with a
variety of byzantine behaviours.

\section{The algorithm}

The algorithm in the book uses no sequence numbers and assumes reliable
communications. It also doesn't discuss what variables need to exist to
store neccessary data. The first step was to adapt this code to an implementable
format. The code for BRB in $BAMP[t < n/3]$ from the book is in figure \ref{code1}.

\begin{figure}[h]
  \includegraphics[width=\textwidth]{pseudo1}
  \caption{Pseudo code from Michel Raynal}
  \label{code1}
\end{figure}

A few things need to be added before implementing it is possible
\begin{itemize}
  \item Sequence numbers for messages.
  \item Some system for retransmissions and acknowledgements.
  \item Variables to track these things and the conditions for the different
    stages of communication.
\end{itemize}

Adding sequence numbers is pretty simple. The system for retransmissions is
basically a do-forever loop running in parallel with the message handler
funtions, which sends messages to processes which have not yet acknowledge
whichever message it is we are currently sending (INIT,ECHO or READY).
This all is stored in a buffer with an entry per open broadcast which is looped
through on every iteration of the do-forever loop. Pseudo code for the final
BRB algorithm is in figure \ref{code2}.

\begin{figure}
\input{code.tex}
  \caption{Final pseudocode}
  \label{code2}
\end{figure}

\newpage
\section{The implementation}

\subsection{Architecture}
Implementing this in rust required a stong foundation. The first step was to
create the networking code and event-loop structure (the actual first step was
reading the rust documentation to learn the language). The goals for the base
system were:

\begin{itemize}
  \item Handling all of the algorithm logic in one thread using a synchronous
    event based structure.
  \item Have support for several event sources transmitting to this thread, to
    facilitate several types of external or asynchronous services, like timers,
    networking or even other forms of IO.
  \item A pattern for creating these interdependent algorithms where events
    can be propagated from the network/event-queue all the way to the higest
    layers of services.
\end{itemize}

Knowing from past experience that mutexes and shared memory are dangerous I
decided to use Multiple Producer, Single Consumer channels from the rust
standard library and have an entirely message based approach to asynchronicity.

Every service that listens to the event queue is registered in a hashmap
with its unique service id as a key. They all implement the EventHandler trait,
which only requires the on\_event callback. This pattern is repeated so for
example any service that uses the network service implements the NetworkHandler
trait (which requires the on\_message function) and any network service in turn
must implement the NetworkService trait and provide the send\_to and broadcast
functions (broadcast here just means send to every known host, not any attempt
at a reliable broadcast).
This results in a kind of tree structure where higher level services register
with the lower level services they depend on. Looking at the declaration of
the service struct from \textit{src/brb\_broadcast/mod.rs} in figure \ref{rust1}
we can see how this works, the BRBBroadcast struct has a dispatch (which is
an int to T hashmap) of BroadcastHandlers and a reference to a network, could
be any type that implements the Network interface but the only one that exists
right now is the BasicNetwork.

Since it all runs in one thread it also guarantees that even if a service
implements several callback traits, for instance if it listens to the network
and has a do-forever loop running, it can be certain their executoins will never
overlap can safely mutate any internal state.

\begin{figure}[h]
  \begin{lstlisting}
    pub struct BRBBroadcast<'a> {
        // interface components
        dispatch: Dispatch<&'a dyn BroadcastHandler>,
        network: &'a dyn Network,
        // constants
        id: u16,
        n: usize,
        t: usize,
        // mutable internal state
        seq: Cell<u32>,
        buffer: RefCell<Buffer>
    }
  \end{lstlisting}
  \caption{BRB Struct Declaration}
  \label{rust1}
\end{figure}

\subsection{The algorithm}

The implementation of the algorithm itself is pretty straight-forward. The
actual rust code does not use an array of sets to keep track of which
messages have been received from who but three separate sets - this is because
it was built on top of an implementation of no-duplicity broadcast which
only used one such set. This is the only real difference between the rust code
and the pseudo code.

The local variables, \textit{seq} and \textit{buffer}, are wrapped in \textbf{Cell} and
\textbf{RefCell} to provide what in rust is called \textit{internal mutability}.
To provide referential safety you cannot have multiple mutable references to the same
object in rust, so if you need several references to an object with
immutable structure but some mutable state you have to use these \textbf{Cell}
mechanisms for run-time \textit{borrow-checking}.

\section{Evaluation}

Evaluating the code turned out to be more complicated than I had anticipated.
To be considered byzantine fault tolerant the algorithm should be able to withstand
malicious processes and even coordinated groups of malicious processes so writing
a good test for this algorithm is tantamount to writing a full on exploit. It
is further complicated by the fact that there is no known vulnerability in the
algorithm! Trying to come up with and implementing a counter-example is
therefore unrealistic, and hopefully impossible. Instead I decided to test it
using three byzantine behaviours in different configurations:

\begin{itemize}
  \item The byzantine processes not participating in the protocol - as if
    they had crashed (process crashes are a form of byzantine behaviour).
  \item The byzantine processes sending randomized messages at a regular interval
    to all other broadcasts.
  \item All the byzantine processes sending the same fake message in an attempt
    to falsify a broadcast.
\end{itemize}

These tests were then run on mininet with n=k*3+1 (k values between 1 and 10)
to make sure it functioned. To see how this in action check out the video!

Interesting to note is that because the algorithm is unbounded and will open
an entry in the buffer for any message it received
\subsection{Functionality}

\textcolor{red}{
DESCRIBING THE TESTS I RAN TO SEE IF I COULD GET IT TO FAIL ON LOCALOHST
AND MININET, WASNT PLANNING ON SHOWING ANY REAL OUTPUT OF TEST ETC HERE
BECAUSE ITS SO UNINTREESTING ILL SAVE THAT FOR THE PRESENTATION/VIDEO
}

\subsection{Performance}

\textcolor{red}{
  ILL TRY TO GET SOME NICE DATA TO SHOW HERE BUT IT WILL PROBABLY BE PRETTY
  UNEXCITING ON ITS OWN - ILL SEE WHAT KIND OF NICE GRAPHS I CAN MAKE OUT OF
  IT
}

\section{Conclusions and further work.}

\textcolor{red}{
  DISCUSSING THE WAY I IMPLEMENTED IT AND THE PROBLEMS WITH TESTING THIS KIND
  OF APPLICATION AND THE WORK I WANT TO DO IN BUILDING THE ARCHITECTURE AND
  IMPLEMENTING MORE ALGORITHMS. I MIGHT WAX POETIC ABOUT MAXIMIZING THE 
  EXPRESSIVENESS OF THE CODE AND MAKING IT EASY TO WORK WITH HERE UNLESS YOU
  THINK IT WOULD ANNOY MAGNUS
}









\end{document}

