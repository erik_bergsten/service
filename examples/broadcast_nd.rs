//use std::cell::RefCell;
//use std::cell::Cell;
use std::env;
//use std::collections::HashMap;
//use std::sync::mpsc::{channel, Receiver, Sender};
use service::message::Message;
//use service::dispatch::Dispatch;
use service::basic_network::BasicNetwork;
use service::hosts::Hosts;
use service::network::Network;
use service::timer::{Timer, TimerHandler};
use service::vessel::{Service, Vessel};
//use service::id;
use service::broadcast::{BroadcastHandler, Broadcast};
use service::nd_broadcast::{NDBroadcast, NDBroadcastMessage};
use service::brb_broadcast::{BRBBroadcast, BRBMessage};

const NAMES: &'static [&'static str] = &["Bowser", "Mario", "Luigi", "Peach", "Toad"];

struct FakeUser<'a> {
    id: u16,
    name: &'static str,
    broadcast: &'a dyn Broadcast,
    network: &'a dyn Network,
}
impl Service for FakeUser<'_> {
    fn id(&self) -> u16 {
        999
    }
    fn name(&self) -> &str {
        "Fake User"
    }
}
impl BroadcastHandler for FakeUser<'_> {
    fn on_broadcast(&self, from: u16, mut msg: Message) {
        let index: usize = from.into();
        let tick = msg.pop_u16().unwrap();
        println!("[{}]: {}/{}", NAMES[index], tick, msg.as_str());
    }
}
impl TimerHandler for FakeUser<'_> {
    fn on_tick(&self, tick: u16) {
        if tick == self.id {
            if self.id != 3 {
                let mut msg = Message::from_str(self.name);
                msg.push_u16(tick);
                msg.push_u16(self.id());
                self.broadcast.init(msg);
            } else {
                let mut truth = Message::from_bytes(b"the truth");
                let mut lie = Message::from_bytes(b"the LIE");
                truth.push_u16(tick);
                lie.push_u16(tick);
                truth.push_u16(self.id());
                lie.push_u16(self.id());
                let msg1 = NDBroadcastMessage::encode(NDBroadcastMessage::INIT(truth, self.id, 0));
                let msg2 = NDBroadcastMessage::encode(NDBroadcastMessage::INIT(lie, self.id, 0));

                println!("Sending msg1: {:?}", msg1);
                println!("Sending msg2: {:?}", msg2);
                self.network.send(0, msg1.clone());
                self.network.send(1, msg1);
                self.network.send(2, msg2);
            }
        }
    }
}
impl<'a> FakeUser<'a> {
    fn new(
        network: &'a dyn Network,
        broadcast: &'a dyn Broadcast,
        id: u16,
    ) -> FakeUser<'a> {
        let index: usize = id.into();
        FakeUser {
            network,
            broadcast,
            id,
            name: NAMES[index],
        }
    }
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let n_hosts: u16 = args[1].parse::<u16>().unwrap();
    let id: u16 = args[2].parse::<u16>().unwrap() - 1;

    println!("Running host {}/{}", id, n_hosts);

    let vessel = Vessel::new(id);
    let hosts = Hosts::new_localhosts(vessel.id, n_hosts);
    hosts.show();
    let network = BasicNetwork::new(vessel.event_tx.clone(), hosts).unwrap();
    vessel.register(&network);
    network.show();

    let timer = Timer::new(vessel.event_tx.clone());
    vessel.register(&timer);

    let broadcast = NDBroadcast::new(&network, vessel.id, n_hosts, 1);
    network.register(&broadcast);
    timer.register(&broadcast);
    broadcast.show();

    let fake_user = FakeUser::new(&network, &broadcast, vessel.id);
    broadcast.register(&fake_user);
    timer.register(&fake_user);

    vessel.run();
}
