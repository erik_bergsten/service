use service::message::Message;
use service::timer::{TimerHandler};
use service::vessel::{Service};
use service::network::Network;
use service::broadcast::{BroadcastHandler, Broadcast};
use service::brb_broadcast::{self, BRBMessage};

use rand::Rng;

use crate::user::NAMES;

pub struct ByzantineUser<'a> {
    id: u16,
    n: u16,
    //broadcast: &'a dyn Broadcast,
    network: &'a dyn Network
}
impl Service for ByzantineUser<'_> {
    fn id(&self) -> u16 {
        66
    }
    fn name(&self) -> &str {
        "ByzantinetUser"
    }
}
impl<'a> ByzantineUser<'a> {
    pub fn new(n: u16, id: u16, network: &'a dyn Network) -> ByzantineUser<'a> {
        ByzantineUser {
            n,
            id,
            //broadcast,
            network
        }
    }
}
fn attack_msg() -> BRBMessage {
    let msg = Message::from_str("PWNED");
    let seq: u32 = 1337;//rng.gen();
    let id: u16 = 322;//rng.gen();
    let msg_type: u8 = brb_broadcast::READY;//rng.gen();
    let constructor: bool = true; //rng.gen();
    let b_msg: BRBMessage;
    if constructor {
        // true == msg, false == ack
        BRBMessage::MSG(msg, msg_type, id, seq)
    }else{
        BRBMessage::ACK(msg, msg_type, id, seq)
    }
}
fn random_msg() -> BRBMessage {
    let mut rng = rand::thread_rng();
    let i: usize = rng.gen::<usize>() % NAMES.len();
    let text: &str = NAMES[i];
    let msg = Message::from_str(text);
    let seq: u32 = rng.gen();
    let id: u16 = rng.gen();
    let msg_type: u8 = rng.gen();
    let constructor: bool = rng.gen();
    let b_msg: BRBMessage;
    if constructor {
        // true == msg, false == ack
        BRBMessage::MSG(msg, msg_type, id, seq)
    }else{
        BRBMessage::ACK(msg, msg_type, id, seq)
    }
}
impl TimerHandler for ByzantineUser<'_> {
    fn on_tick(&self, tick: usize, _evt: u16){
        let b_msg = random_msg();
        println!("Sending out the evil message: {:?}", b_msg);
        let encoded = BRBMessage::to_network(b_msg);
        self.network.broadcast(encoded);
    }
}
impl BroadcastHandler for ByzantineUser<'_> {
    fn on_broadcast(&self, from: u16, msg: Message){
        println!("Got: {} from {}", msg.as_str(), from);
    }
}
