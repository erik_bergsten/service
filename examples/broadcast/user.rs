use service::message::Message;
//use service::network::Network;
use service::timer::{TimerHandler};
use service::vessel::{Service};
use service::network_stats::NetworkStats;
use service::broadcast::{BroadcastHandler, Broadcast};
use std::cell::Cell;
pub const NAMES: &'static [&'static str] = &["Bowser", "Mario", "Luigi", "Peach", "Toad", "Wario", "Waluigi", "Koopa", "Daisy", "Yoshi", "Goomba", "Bob-omb"];

pub struct BroadcastUser<'a> {
    id: u16,
    n: u16,
    broadcast: &'a dyn Broadcast,
    stats: &'a NetworkStats<'a>,
    delivered: Cell<usize>
}
impl Service for BroadcastUser<'_> {
    fn id(&self) -> u16 {
        66
    }
    fn name(&self) -> &str {
        "BroadcastUser"
    }
}
impl<'a> BroadcastUser<'a> {
    pub fn new(n: u16, id: u16, broadcast: &'a dyn Broadcast, stats: &'a NetworkStats<'a>) -> BroadcastUser<'a> {
        BroadcastUser {
            n,
            id,
            broadcast,
            stats,
            delivered: Cell::new(0)
        }
    }
}
impl TimerHandler for BroadcastUser<'_> {
    fn on_tick(&self, tick: usize, _event: u16){
        if (tick as u16) % self.n == self.id {
            println!("Ticking {}", tick);
            let mut msg = Message::from_str(NAMES[(self.id as usize) % NAMES.len()]);
            msg.push_u16(self.id());
            self.broadcast.init(msg);
        }
    }
}
impl BroadcastHandler for BroadcastUser<'_> {
    fn on_broadcast(&self, from: u16, msg: Message){
        let delivered = self.delivered.get()+1;
        self.delivered.set(delivered);
        println!("Got #{}: {} from {}", delivered, msg.as_str(), from);
        println!("With {} msgs sent and {} msgs received.", self.stats.sent(), self.stats.received());
    }
}
