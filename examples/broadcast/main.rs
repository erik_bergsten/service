use std::env;
use service::basic_network::BasicNetwork;
use service::hosts::Hosts;
use service::timer::{Timer};
use service::vessel::{Service, Vessel};
use service::network_stats::NetworkStats;
//use service::broadcast::{BroadcastHandler, Broadcast};
use service::brb_broadcast::{BRBBroadcast};

//mod byzantine;
mod user;
use user::BroadcastUser;

mod byzantine;
use byzantine::ByzantineUser;

fn main() {
    let args: Vec<String> = env::args().collect();
    let n_hosts: u16 = args[2].parse::<u16>().unwrap();
    let id: u16 = args[3].parse::<u16>().unwrap() - 1;


    println!("Running host {}/{}", id, n_hosts);
    let vessel = Vessel::new(id);

    let hosts: Hosts;
    if args[1] == "mininet" {
        hosts = Hosts::new_minihosts(vessel.id, n_hosts);
    }else{
        hosts = Hosts::new_localhosts(vessel.id, n_hosts);
    }
    hosts.show();
    let network = BasicNetwork::new(vessel.event_tx.clone(), hosts).unwrap();
    vessel.register(&network);
    network.show();

    let timer = Timer::new(vessel.event_tx.clone(), 100);
    vessel.register(&timer);

    let t: usize = (n_hosts as usize) / 3;
    println!("T = {}", t);



    if false { //id < (t as u16) {
        println!("I'm byzantine!");
        let b_user = ByzantineUser::new(n_hosts, vessel.id, &network);
        timer.register(&b_user, 1000, 0);
        //broadcast.register(&b_user);
        vessel.run();
    }else{
        println!("I'm normal!");
        let stats = NetworkStats::new(&network);
        let broadcast = BRBBroadcast::new(&stats, vessel.id, n_hosts as usize, t);
        stats.register(&broadcast);
        network.register(&stats);
        timer.register(&broadcast, 1000, 0);
        broadcast.show();
        let user = BroadcastUser::new(n_hosts, vessel.id, &broadcast, &stats);
        timer.register(&user, 1000, 0);
        broadcast.register(&user);
        vessel.run();
    }

}
