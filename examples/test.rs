use std::net::{IpAddr, Ipv4Addr, SocketAddr};
use std::result;
use std::error::Error;
use std::fs;

fn main() {
    println!("hihi");
    let text = fs::read_to_string("hosts/mininet.txt").unwrap();
    for (i, line) in text.lines().enumerate() {
        let address: SocketAddr = line.parse().unwrap();
        println!("address #{}: {:?}", i, address);
    }
}
