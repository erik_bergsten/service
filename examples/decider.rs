use std::env;
use service::basic_network::BasicNetwork;
use service::hosts::Hosts;
use service::timer::{Timer,TimerHandler};
use service::vessel::{Service, Vessel};
use service::message::Message;
use service::network::{Network,NetworkHandler};
//use service::nd_broadcast::NDBroadcast;
use service::network_stats::NetworkStats;
use service::broadcast::{BroadcastHandler,
                         Broadcast,
                         BEBroadcast,
                         BRBBroadcast,
                         NDBroadcast};
use service::consensus::{BinaryConsensus,
                         BinaryConsensusSeeker,
                         BBConsensus};
//use service::brb_broadcast::{BRBBroadcast};
use serde::{Deserialize,Serialize};
use serde_json::Result;
use std::cell::Cell;
use std::time::{Instant, Duration};

const TIMEOUT: u64 = 1000;


struct Decider<'a> {
    vessel: &'a Vessel<'a>,
    consensus: &'a BBConsensus<'a>,
    decision: Cell<usize>
}
impl Service for Decider<'_> {
    fn id(&self) -> u16 {
        0xff
    }
}
impl<'a> Decider<'a> {
    
}
impl TimerHandler for Decider<'_> {
    fn on_tick(&self, tick: usize, event: u16) {
        if tick == 2 {
            println!("dingdongdingdong!");
            let msg = Message::from_str("AAAA");
            self.consensus.propose(msg, true, self.id());
        }
    }
}
impl BinaryConsensusSeeker for Decider<'_> {
    fn on_consensus(&self, prop: Message, decision: bool) {
        println!("A decision has been made! {} is {}", prop.as_str(), decision);
        let decision = self.decision.get();
        self.decision.set(decision+1);
        let msg = Message::from_string(format!("(prop {})", decision));
        self.consensus.propose(msg, self.vessel.id < 2, self.id());
    }
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let n_hosts: u16 = args[2].parse::<u16>().unwrap();
    let t = (((n_hosts as f32)/3.0 as f32).ceil() as u16) - 1;
    let id: u16 = args[3].parse::<u16>().unwrap() - 1;
    let vessel = Vessel::new(id);
    let hosts: Hosts;
    if args[1] == "mininet" {
        hosts = Hosts::new_minihosts(vessel.id, n_hosts);
    }else{
        hosts = Hosts::new_localhosts(vessel.id, n_hosts);
    }


    let vessel = Vessel::new(id);
    let network = BasicNetwork::new(vessel.event_tx.clone(), hosts).unwrap();
    vessel.register(&network);
    let stats = NetworkStats::new(&network);

    let timer = Timer::new(&vessel, TIMEOUT);
    vessel.register(&timer);

    let broadcast = NDBroadcast::new(&vessel, &stats, t);
    network.register(&broadcast);
    timer.register(&broadcast, 0, TIMEOUT);

    let consensus = BBConsensus::new(&vessel, &network, &broadcast, t);
    broadcast.register(&consensus);

    let decider = Decider {
        vessel: &vessel,
        //broadcast: &broadcast
        consensus: &consensus,
        decision: Cell::new(0)
    };
    timer.register(&decider, 0, TIMEOUT);
    consensus.register(&decider);

    vessel.run();

}
