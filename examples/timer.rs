use std::env;
//use service::basic_network::BasicNetwork;
//use service::hosts::Hosts;
use service::timer::{Timer,TimerHandler};
use service::vessel::{Service, Vessel};
use std::time::Duration;
//use service::network_stats::NetworkStats;
//use service::broadcast::{BroadcastHandler, Broadcast};
//use service::brb_broadcast::{BRBBroadcast};

//mod user;
//use user::BroadcastUser;
//mod byzantine;
//use byzantine::ByzantineUser;


struct MyUser(&'static str);
impl Service for MyUser {
    fn id(&self) -> u16 {
        1234
    }
}
impl TimerHandler for MyUser {
    fn on_tick(&self, tick: usize, event: u16) {
        println!("{} got tick #{} of type {}!", self.0, tick, event);
    }
}
fn main() {
    //let args: Vec<String> = env::args().collect();

    let vessel = Vessel::new(0);


    //let network = BasicNetwork::new(vessel.event_tx.clone(), hosts).unwrap();
    //vessel.register(&network);

    let timer = Timer::new(vessel.event_tx.clone(),
                           100);
    vessel.register(&timer);


    let user = MyUser("Ivan the Great");
    timer.register(&user, 0, 1000);
    timer.register(&user, 1337, 500);
    vessel.run();

}
