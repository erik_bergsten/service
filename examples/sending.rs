use std::sync::mpsc::{channel, Receiver, Sender};
use std::thread;
use std::net::{UdpSocket, SocketAddr};
use std::time::{Instant};
use std::sync::Arc;

const TEXT: &str = "Some cool text of a real cool length with real cool content for a real cool test by a real cool guy in a real cool house in a real cool town with some real cool vibes.\nSome cool text of a real cool length with real cool content for a real cool test by a real cool guy in a real cool house in a real cool town with some real cool vibes.\nSome cool text of a real cool length with real cool content for a real cool test by a real cool guy in a real cool house in a real cool town with some real cool vibes.\nSome cool text of a real cool length with real cool content for a real cool test by a real cool guy in a real cool house in a real cool town with some real cool vibes.\nSome cool text of a real cool length with real cool content for a real cool test by a real cool guy in a real cool house in a real cool town with some real cool vibes.\nSome cool text of a real cool length with real cool content for a real cool test by a real cool guy in a real cool house in a real cool town with some real cool vibes.\n";
//type Message = Arc<Vec<u8>>;
type Message = Vec<u8>;

fn main() {
    println!("Sending and stuff!");
    let (sender, receiver): (Sender<Message>, Receiver<Message>) = channel();

    thread::spawn(move || {
        let socket = UdpSocket::bind("127.0.0.1:4000").unwrap();
        let to_addr = SocketAddr::from(([127, 0, 0, 1], 4444));
        loop {
            let msg = receiver.recv().unwrap();
            socket.send_to(msg.as_slice(), to_addr).unwrap();
        }
    });

    //let msg = Arc::new(TEXT.as_bytes().to_vec());
    let msg = TEXT.as_bytes().to_vec();
    println!("Message legnth: {}", msg.len());
    let before = Instant::now();
    for i in 0..1000 {
        sender.send(msg.clone()).unwrap();
    }
    let after = Instant::now();
    println!("Done in {:?}!", after.duration_since(before));



}
