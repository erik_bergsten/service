use std::env;
use service::basic_network::BasicNetwork;
use service::hosts::Hosts;
use service::timer::{Timer,TimerHandler};
use service::vessel::{Service, Vessel};
use service::message::Message;
use service::network::{Network,NetworkHandler};
//use service::nd_broadcast::NDBroadcast;
use service::network_stats::NetworkStats;
use service::broadcast::{BroadcastHandler,
                         Broadcast,
                         BEBroadcast,
                         BRBBroadcast,
                         NDBroadcast};
//use service::brb_broadcast::{BRBBroadcast};
use serde::{Deserialize,Serialize};
use serde_json::Result;
use std::cell::Cell;
use std::time::{Instant, Duration};

const TIMEOUT: u64 = 500;

struct Sender<'a> {
    vessel: &'a Vessel<'a>,
    network: &'a dyn Network,
    broadcast: &'a dyn Broadcast,
    ready: Cell<bool>,
    received: Cell<u32>,
    stats: &'a NetworkStats<'a>,
    start: Cell<Instant>
}
impl Service for Sender<'_> {
    fn id(&self) -> u16 {
        1234
    }
}

pub fn encode(msg: &Message, constructor: u8, id: u16, seq: u32) -> Message {
    let mut out = msg.clone();
    out.push_u8(constructor);
    out.push_u16(id);
    out.push_u32(seq);
    out
}
impl TimerHandler for Sender<'_> {
    fn on_tick(&self, tick: usize, event: u16) {
        if tick == 4 && self.ready.get() {
            let msg = Message::from_string(format!("{} says {} for the {}th time", self.vessel.id, "hello world", 0));
            self.broadcast.init(msg, self.id());
            self.start.set(Instant::now());
            self.ready.set(false);
        }/* else if tick == 10 && self.vessel.id == 0 {
            let mut src = Message::from_str("WRONG");
            src.push_u16(self.id());
            let mut msg = encode(&src, 0, 0, 1);
            self.network.send(1, msg, self.broadcast.id());
        }*/
    }
}

impl BroadcastHandler for Sender<'_> {
    fn on_broadcast(&self, msg: Message, from: u16, seq: u32){
        let received = self.received.get();
        let start = self.start.get();
        let delta = start.elapsed().as_millis();
        self.received.set(received+1);
        //println!("Got {}/{}: {} from {}", received, seq, msg.as_str(), from);
        //println!("{},{},{},{},{}", "msg", received, seq, self.stats.sent(), delta);
        println!("Delivered [{}] from {} after {} ms!", msg.as_str(), from, delta);
        if from == self.vessel.id && received < 10 {
            //self.ready.set(true);
            let msg = Message::from_string(format!("Count to {}!", seq));
            self.broadcast.init(msg, self.id());
            //self.ready.set(false);
            self.start.set(Instant::now());
        }
    }
}
fn main() {
    let args: Vec<String> = env::args().collect();
    let n_hosts: u16 = args[2].parse::<u16>().unwrap();
    let t = (((n_hosts as f32)/3.0 as f32).ceil() as u16) - 1;
    let id: u16 = args[3].parse::<u16>().unwrap() - 1;
    //println!("Running host {} with n={} with t={}", id+1, n_hosts, t);
    let vessel = Vessel::new(id);
    let hosts: Hosts;
    if args[1] == "mininet" {
        hosts = Hosts::new_minihosts(vessel.id, n_hosts);
    }else if args[1] == "planetlab" {
        hosts = Hosts::load_hosts(id, "hosts/planetlab.txt").unwrap();
    }else {
        hosts = Hosts::new_localhosts(vessel.id, n_hosts);
    }

    //hosts.show();
    println!("msg,broadcast,seq,sent,delay");

    let vessel = Vessel::new(id);
    let network = BasicNetwork::new(vessel.event_tx.clone(), hosts).unwrap();
    vessel.register(&network);
    let stats = NetworkStats::new(&network);

    let timer = Timer::new(&vessel, TIMEOUT);
    vessel.register(&timer);

    //let broadcast = BEBroadcast::new(&vessel, &network);
    let broadcast = NDBroadcast::new(&vessel, &stats, t);
    stats.register(&broadcast);
    network.register(&stats);
    timer.register(&broadcast,0, TIMEOUT);

    let sender = Sender {
        vessel: &vessel,
        broadcast: &broadcast,
        network: &network,
        ready: Cell::new(id == 0),
        received: Cell::new(0),
        stats: &stats,
        start: Cell::new(Instant::now())
    };
    timer.register(&sender, 0, TIMEOUT);
    broadcast.register(&sender);

    vessel.run();

}
